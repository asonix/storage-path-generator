use storage_path_generator::{Generator, Path};

#[test]
fn invalid_paths() {
    let paths = [
        vec![0, 1],
        vec![1],
        vec![2, 1],
        vec![3, 1, 1],
        vec![1, 1000],
        vec![2, 0, 1000],
        vec![2, 1000, 0],
    ];

    for path in paths {
        assert!(Path::from_vec(path).is_err());
    }
}

#[test]
fn generates_valid_paths() {
    let generator = Generator::new();

    for _ in 0..1_000_000 {
        let path = generator.next();
        Path::from_vec(path.into_inner()).unwrap();
    }
}

#[test]
fn round_trip() {
    const VALUES: [u16; 4] = [0, 30, 450, 999];

    fn do_build(depth: u16, existing: Vec<u16>) -> Box<dyn Iterator<Item = Vec<u16>>> {
        if depth > 0 {
            let left = VALUES.iter().flat_map(move |value| {
                let mut new_vec = existing.clone();
                new_vec.push(*value);
                do_build(depth - 1, new_vec)
            });

            Box::new(left)
        } else {
            Box::new(vec![existing].into_iter())
        }
    }

    fn build(max_depth: u16) -> impl Iterator<Item = Vec<u16>> {
        (0..max_depth).flat_map(|i| do_build(i, vec![i]))
    }

    for vec in build(8) {
        let path = Path::from_vec(vec).unwrap();

        let bytes = path.to_be_bytes();
        let new_path = Path::from_be_bytes(bytes).unwrap();

        assert_eq!(new_path, path);
    }
}

#[test]
fn first_level_iter() {
    let generator = Generator::new();
    let zero = generator.next().into_inner();
    let one = generator.next().into_inner();
    let two = generator.next().into_inner();

    let expect_zero = vec![0];
    let expect_one = vec![1, 0];
    let expect_two = vec![1, 1];

    assert_eq!(zero, expect_zero);
    assert_eq!(one, expect_one);
    assert_eq!(two, expect_two);
}

#[test]
fn third_level_iter() {
    let generator = Generator::from_existing(Path::from_vec(vec![2, 0, 998]).unwrap());
    let zero = generator.next().into_inner();
    let one = generator.next().into_inner();
    let two = generator.next().into_inner();

    let expect_zero = vec![2, 0, 999];
    let expect_one = vec![2, 1, 0];
    let expect_two = vec![2, 1, 1];

    assert_eq!(zero, expect_zero);
    assert_eq!(one, expect_one);
    assert_eq!(two, expect_two);
}

#[test]
fn third_level_iter_two() {
    let generator = Generator::from_existing(Path::from_vec(vec![2, 500, 998]).unwrap());
    let zero = generator.next().to_strings();
    let one = generator.next().to_strings();
    let two = generator.next().to_strings();

    let expect_zero = vec!["002", "500", "999"];
    let expect_one = vec!["002", "501", "000"];
    let expect_two = vec!["002", "501", "001"];

    assert_eq!(zero, expect_zero);
    assert_eq!(one, expect_one);
    assert_eq!(two, expect_two);
}

#[test]
fn fourth_level_iter() {
    let generator = Generator::from_existing(Path::from_vec(vec![3, 0, 999, 998]).unwrap());
    let zero = generator.next().to_strings();
    let one = generator.next().to_strings();
    let two = generator.next().to_strings();

    let expect_zero = vec!["003", "000", "999", "999"];
    let expect_one = vec!["003", "001", "000", "000"];
    let expect_two = vec!["003", "001", "000", "001"];

    assert_eq!(zero, expect_zero);
    assert_eq!(one, expect_one);
    assert_eq!(two, expect_two);
}

#[test]
fn fourth_level_iter_two() {
    let generator = Generator::from_existing(Path::from_vec(vec![3, 0, 500, 998]).unwrap());
    let zero = generator.next().to_strings();
    let one = generator.next().to_strings();
    let two = generator.next().to_strings();

    let expect_zero = vec!["003", "000", "500", "999"];
    let expect_one = vec!["003", "000", "501", "000"];
    let expect_two = vec!["003", "000", "501", "001"];

    assert_eq!(zero, expect_zero);
    assert_eq!(one, expect_one);
    assert_eq!(two, expect_two);
}

#[test]
fn digit_transitions() {
    let generator = Generator::from_existing(Path::from_vec(vec![1, 8]).unwrap());

    let nine = generator.next().to_strings();
    let ten = generator.next().to_strings();

    let expect_nine = vec!["001", "009"];
    let expect_ten = vec!["001", "010"];

    assert_eq!(nine, expect_nine);
    assert_eq!(ten, expect_ten);

    let generator = Generator::from_existing(Path::from_vec(vec![1, 98]).unwrap());

    let ninety_nine = generator.next().to_strings();
    let one_hundred = generator.next().to_strings();

    let expect_ninety_nine = vec!["001", "099"];
    let expect_one_hundred = vec!["001", "100"];

    assert_eq!(ninety_nine, expect_ninety_nine);
    assert_eq!(one_hundred, expect_one_hundred);
}

#[test]
fn second_level_transition() {
    let generator = Generator::new();

    let first_level = generator.next().to_strings();
    let second_level = generator.next().to_strings();

    let expect_first_level = vec!["000"];
    let expect_second_level = vec!["001", "000"];

    assert_eq!(first_level, expect_first_level);
    assert_eq!(second_level, expect_second_level);
}

#[test]
fn third_level_transition() {
    let generator = Generator::from_existing(Path::from_vec(vec![1, 998]).unwrap());

    let second_level = generator.next().to_strings();
    let third_level = generator.next().to_strings();

    let expect_second_level = vec!["001", "999"];
    let expect_third_level = vec!["002", "000", "000"];

    assert_eq!(second_level, expect_second_level);
    assert_eq!(third_level, expect_third_level);
}

#[test]
fn fourth_level_transition() {
    let generator = Generator::from_existing(Path::from_vec(vec![2, 999, 998]).unwrap());

    let third_level = generator.next().to_strings();
    let fourth_level = generator.next().to_strings();

    let expect_third_level = vec!["002", "999", "999"];
    let expect_fourth_level = vec!["003", "000", "000", "000"];

    assert_eq!(third_level, expect_third_level);
    assert_eq!(fourth_level, expect_fourth_level);
}

#[test]
fn fifth_level_transition() {
    let generator = Generator::from_existing(Path::from_vec(vec![3, 999, 999, 998]).unwrap());

    let fourth_level = generator.next().to_strings();
    let fifth_level = generator.next().to_strings();

    let expect_fourth_level = vec!["003", "999", "999", "999"];
    let expect_fifth_level = vec!["004", "000", "000", "000", "000"];

    assert_eq!(fourth_level, expect_fourth_level);
    assert_eq!(fifth_level, expect_fifth_level);
}
